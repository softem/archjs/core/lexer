var fs = require('fs-extra');

if( !process.env.npm_config_ArchjsLexerDev ){
    try {
        var distFiles = fs.readdirSync('./dist');
        var devFiles  = fs.readdirSync('./');
        distFiles.forEach( (val, key) => fs.copySync('./dist/' + val, "./" + val) );

        devFiles.filter( (val)=> val !== 'package.json' )
                .forEach((val)=> fs.remove('./' + val ) );
    }catch (e) { /*ignore any error*/ }
}
